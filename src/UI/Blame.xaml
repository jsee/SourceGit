<Window x:Class="SourceGit.UI.Blame"
        x:Name="me"
        xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
        xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
        xmlns:d="http://schemas.microsoft.com/expression/blend/2008"
        xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
        mc:Ignorable="d"
        Title="Blame"
        Height="600" Width="800">

    <!-- Enable WindowChrome -->
    <WindowChrome.WindowChrome>
        <WindowChrome UseAeroCaptionButtons="False" CornerRadius="0" CaptionHeight="32"/>
    </WindowChrome.WindowChrome>

    <!-- Window Content -->
    <Border Background="{StaticResource Brush.BG1}">
        <!-- Fix Maximize BUG -->
        <Border.Style>
            <Style TargetType="{x:Type Border}">
                <Style.Triggers>
                    <DataTrigger Binding="{Binding WindowState, ElementName=me}" Value="Maximized">
                        <Setter Property="Margin" Value="6"/>
                    </DataTrigger>
                    <DataTrigger Binding="{Binding WindowState, ElementName=me}" Value="Normal">
                        <Setter Property="Margin" Value="0"/>
                    </DataTrigger>
                </Style.Triggers>
            </Style>
        </Border.Style>

        <Grid>
            <Grid.RowDefinitions>
                <RowDefinition Height="32"/>
                <RowDefinition Height="24"/>
                <RowDefinition Height="*"/>
            </Grid.RowDefinitions>

            <!-- Title bar -->
            <Grid Grid.Row="0" Background="{StaticResource Brush.BG4}">
                <Grid.ColumnDefinitions>
                    <ColumnDefinition Width="Auto"/>
                    <ColumnDefinition Width="*"/>
                    <ColumnDefinition Width="Auto"/>
                </Grid.ColumnDefinitions>

                <!-- Logo & TITLE -->
                <StackPanel Grid.Column="0" Orientation="Horizontal">
                    <Path
                        Width="20" Height="20" Margin="6,-1,2,0"
                        Style="{StaticResource Style.Icon}" 
                        Data="{StaticResource Icon.Git}" 
                        Fill="#FFF05133"
                        WindowChrome.IsHitTestVisibleInChrome="True"
                        MouseLeftButtonDown="LogoMouseButtonDown"/>
                    <Label Content="SOURCE GIT - BLAME" FontWeight="Light"/>
                </StackPanel>

                <!-- Options -->
                <StackPanel Grid.Column="2" Orientation="Horizontal" WindowChrome.IsHitTestVisibleInChrome="True">
                    <Button Click="Minimize" Width="32" Style="{StaticResource Style.Button.HighlightHover}">
                        <Path Style="{StaticResource Style.WindowControlIcon}" Data="{StaticResource Icon.Minimize}"/>
                    </Button>
                    <Button Click="MaximizeOrRestore" Width="32" Style="{StaticResource Style.Button.HighlightHover}">
                        <Path>
                            <Path.Style>
                                <Style TargetType="{x:Type Path}" BasedOn="{StaticResource Style.WindowControlIcon}">
                                    <Style.Triggers>
                                        <DataTrigger Binding="{Binding WindowState, ElementName=me}" Value="Maximized">
                                            <Setter Property="Data" Value="{StaticResource Icon.Restore}"/>
                                        </DataTrigger>
                                        <DataTrigger Binding="{Binding WindowState, ElementName=me}" Value="Normal">
                                            <Setter Property="Data" Value="{StaticResource Icon.Maximize}"/>
                                        </DataTrigger>
                                    </Style.Triggers>
                                </Style>
                            </Path.Style>
                        </Path>
                    </Button>
                    <Button Click="Quit" Width="32">
                        <Button.Style>
                            <Style TargetType="{x:Type Button}" BasedOn="{StaticResource Style.Button.HighlightHover}">
                                <Style.Triggers>
                                    <Trigger Property="IsMouseOver" Value="True">
                                        <Setter Property="Background" Value="Red"/>
                                    </Trigger>
                                </Style.Triggers>
                            </Style>
                        </Button.Style>

                        <Path Width="10" Style="{StaticResource Style.Icon}" Data="{StaticResource Icon.Close}"/>
                    </Button>
                </StackPanel>
            </Grid>

            <!-- Blame file -->
            <Border Grid.Row="1" Padding="2,0">
                <Grid>
                    <Grid.ColumnDefinitions>
                        <ColumnDefinition Width="*"/>
                        <ColumnDefinition Width="Auto"/>
                    </Grid.ColumnDefinitions>

                    <Label Grid.Column="0" x:Name="blameFile" HorizontalAlignment="Left" FontSize="11" Foreground="{StaticResource Brush.FG2}" FontFamily="Consolas"/>
                    <Label Grid.Column="1" HorizontalAlignment="Right" Foreground="{StaticResource Brush.FG2}" FontSize="11" Content="Use right mouse button to view commit information."/>
                </Grid>
            </Border>

            <!-- Content -->
            <Border Grid.Row="2" BorderThickness="1" BorderBrush="{StaticResource Brush.Border2}" ClipToBounds="True">
                <Grid>
                    <Grid.ColumnDefinitions>
                        <ColumnDefinition Width="Auto"/>
                        <ColumnDefinition Width="1"/>
                        <ColumnDefinition Width="*"/>
                    </Grid.ColumnDefinitions>

                    <TextBox
                            x:Name="lineNumber"
                            Grid.Column="0"
                            AcceptsReturn="True"
                            AcceptsTab="True"
                            BorderThickness="0"
                            Background="Transparent"
                            IsReadOnly="True"
                            Margin="4,0,4,0"                   
                            FontSize="13"
                            HorizontalContentAlignment="Right"
                            VerticalAlignment="Stretch"
                            FontFamily="Consolas"/>

                    <Rectangle Grid.Column="1" Width="1" Fill="{StaticResource Brush.Border2}"/>

                    <RichTextBox
                            x:Name="content"
                            Grid.Column="2"
                            AcceptsReturn="True"
                            AcceptsTab="True"
                            IsReadOnly="True"
                            BorderThickness="0"
                            Background="Transparent"
                            Foreground="{StaticResource Brush.FG}"
                            Height="Auto"
                            FontSize="13"
                            HorizontalScrollBarVisibility="Auto"
                            VerticalScrollBarVisibility="Auto"
                            RenderOptions.ClearTypeHint="Enabled"
                            ScrollViewer.ScrollChanged="SyncScrollChanged"
                            PreviewMouseWheel="MouseWheelOnContent"
                            SizeChanged="ContentSizeChanged"
                            SelectionChanged="ContentSelectionChanged"
                            HorizontalAlignment="Stretch"
                            VerticalAlignment="Stretch"
                            FontFamily="Consolas">
                        <RichTextBox.ContextMenu>
                            <ContextMenu>
                                <MenuItem Command="ApplicationCommands.Copy"/>
                            </ContextMenu>
                        </RichTextBox.ContextMenu>
                        <FlowDocument PageWidth="0"/>
                    </RichTextBox>

                    <!-- Loading tip -->
                    <Path x:Name="loading" Grid.ColumnSpan="5" Data="{StaticResource Icon.Loading}" RenderTransformOrigin=".5,.5">
                        <Path.RenderTransform>
                            <RotateTransform Angle="0"/>
                        </Path.RenderTransform>

                        <Path.Style>
                            <Style BasedOn="{StaticResource Style.Icon}" TargetType="{x:Type Path}">
                                <Setter Property="Width" Value="48"/>
                                <Setter Property="Height" Value="48"/>
                                <Setter Property="HorizontalAlignment" Value="Center"/>
                                <Setter Property="VerticalAlignment" Value="Center"/>
                                <Setter Property="Fill" Value="{StaticResource Brush.FG2}"/>
                            </Style>
                        </Path.Style>
                    </Path>

                    <!-- Popup to show commit info -->
                    <Popup x:Name="popup" Grid.ColumnSpan="5" Placement="MousePoint" IsOpen="False" StaysOpen="False" Focusable="True">
                        <Border BorderBrush="{StaticResource Brush.Accent1}" BorderThickness="1" Background="{StaticResource Brush.BG1}">
                            <Grid Margin="4">
                                <Grid.RowDefinitions>
                                    <RowDefinition Height="24"/>
                                    <RowDefinition Height="24"/>
                                    <RowDefinition Height="24"/>
                                </Grid.RowDefinitions>

                                <Grid.ColumnDefinitions>
                                    <ColumnDefinition Width="Auto"/>
                                    <ColumnDefinition Width="Auto"/>
                                </Grid.ColumnDefinitions>

                                <Label Grid.Row="0" Grid.Column="0" Content="COMMIT SHA" Foreground="{StaticResource Brush.FG2}"/>
                                <Label Grid.Row="0" Grid.Column="1" x:Name="commitID"/>
                                <Label Grid.Row="1" Grid.Column="0" Content="AUTHOR" Foreground="{StaticResource Brush.FG2}"/>
                                <Label Grid.Row="1" Grid.Column="1" x:Name="authorName"/>
                                <Label Grid.Row="2" Grid.Column="0" Content="MODIFY TIME" Foreground="{StaticResource Brush.FG2}"/>
                                <Label Grid.Row="2" Grid.Column="1" x:Name="authorTime"/>
                            </Grid>
                        </Border>
                    </Popup>
                </Grid>
            </Border>
        </Grid>
    </Border>
</Window>
