<Window x:Class="SourceGit.UI.InteractiveRebase"
        x:Name="me"
        xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
        xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
        xmlns:d="http://schemas.microsoft.com/expression/blend/2008"
        xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
        xmlns:local="clr-namespace:SourceGit.UI"
        xmlns:helpers="clr-namespace:SourceGit.Helpers"
        mc:Ignorable="d"        
        Title="Interactive Rebase"
        Height="600" Width="800">

    <!-- Enable WindowChrome -->
    <WindowChrome.WindowChrome>
        <WindowChrome UseAeroCaptionButtons="False" CornerRadius="0" CaptionHeight="32"/>
    </WindowChrome.WindowChrome>

    <!-- Window Content -->
    <Border Background="{StaticResource Brush.BG1}">
        <!-- Fix Maximize BUG -->
        <Border.Style>
            <Style TargetType="{x:Type Border}">
                <Style.Triggers>
                    <DataTrigger Binding="{Binding WindowState, ElementName=me}" Value="Maximized">
                        <Setter Property="Margin" Value="6"/>
                    </DataTrigger>
                    <DataTrigger Binding="{Binding WindowState, ElementName=me}" Value="Normal">
                        <Setter Property="Margin" Value="0"/>
                    </DataTrigger>
                </Style.Triggers>
            </Style>
        </Border.Style>

        <Grid>
            <Grid.RowDefinitions>
                <RowDefinition Height="32"/>
                <RowDefinition Height="*"/>
                <RowDefinition Height="1"/>
                <RowDefinition Height="*"/>
                <RowDefinition Height="40"/>
            </Grid.RowDefinitions>

            <!-- Title bar -->
            <Grid Grid.Row="0" Background="{StaticResource Brush.BG4}">
                <Grid.ColumnDefinitions>
                    <ColumnDefinition Width="Auto"/>
                    <ColumnDefinition Width="*"/>
                    <ColumnDefinition Width="Auto"/>
                </Grid.ColumnDefinitions>

                <!-- Logo & TITLE -->
                <StackPanel Grid.Column="0" Orientation="Horizontal">
                    <Path
                        Width="20" Height="20" Margin="6,-1,2,0"
                        Style="{StaticResource Style.Icon}" 
                        Data="{StaticResource Icon.Git}" 
                        Fill="#FFF05133"
                        WindowChrome.IsHitTestVisibleInChrome="True"
                        MouseLeftButtonDown="LogoMouseButtonDown"/>
                    <Label Content="SOURCE GIT - INTERACTIVE REBASE" FontWeight="Light"/>
                </StackPanel>

                <!-- Options -->
                <StackPanel Grid.Column="2" Orientation="Horizontal" WindowChrome.IsHitTestVisibleInChrome="True">
                    <Button Click="Minimize" Width="32" Style="{StaticResource Style.Button.HighlightHover}">
                        <Path Style="{StaticResource Style.WindowControlIcon}" Data="{StaticResource Icon.Minimize}"/>
                    </Button>
                    <Button Click="MaximizeOrRestore" Width="32" Style="{StaticResource Style.Button.HighlightHover}">
                        <Path>
                            <Path.Style>
                                <Style TargetType="{x:Type Path}" BasedOn="{StaticResource Style.WindowControlIcon}">
                                    <Style.Triggers>
                                        <DataTrigger Binding="{Binding WindowState, ElementName=me}" Value="Maximized">
                                            <Setter Property="Data" Value="{StaticResource Icon.Restore}"/>
                                        </DataTrigger>
                                        <DataTrigger Binding="{Binding WindowState, ElementName=me}" Value="Normal">
                                            <Setter Property="Data" Value="{StaticResource Icon.Maximize}"/>
                                        </DataTrigger>
                                    </Style.Triggers>
                                </Style>
                            </Path.Style>
                        </Path>
                    </Button>
                    <Button Click="Quit" Width="32">
                        <Button.Style>
                            <Style TargetType="{x:Type Button}" BasedOn="{StaticResource Style.Button.HighlightHover}">
                                <Style.Triggers>
                                    <Trigger Property="IsMouseOver" Value="True">
                                        <Setter Property="Background" Value="Red"/>
                                    </Trigger>
                                </Style.Triggers>
                            </Style>
                        </Button.Style>

                        <Path Width="10" Style="{StaticResource Style.Icon}" Data="{StaticResource Icon.Close}"/>
                    </Button>
                </StackPanel>
            </Grid>

            <!-- Commit List -->
            <ListView Grid.Row="1"
                      x:Name="commitList"
                      Background="Transparent"
                      Style="{StaticResource Style.ListView.Borderless}"
                      ItemsSource="{Binding ElementName=me, Path=Items}"
                      ScrollViewer.HorizontalScrollBarVisibility="Disabled"
                      ScrollViewer.VerticalScrollBarVisibility="Auto"
                      SelectionChanged="CommitSelectionChanged"
                      BorderThickness="0">
                <ListView.ItemTemplate>
                    <DataTemplate DataType="{x:Type local:InteractiveRebaseItem}">
                        <Grid>
                            <Grid.ColumnDefinitions>
                                <ColumnDefinition Width="80"/>
                                <ColumnDefinition Width="*"/>
                                <ColumnDefinition Width="80"/>
                                <ColumnDefinition Width="128"/>
                                <ColumnDefinition Width="Auto"/>
                            </Grid.ColumnDefinitions>

                            <ComboBox
                                Grid.Column="0" 
                                ItemsSource="{Binding Source={x:Static local:InteractiveRebaseModeInfo.Supported}}"
                                SelectedIndex="{Binding Path=Mode, Mode=TwoWay}"
                                BorderThickness="0">
                                <ComboBox.ItemTemplate>
                                    <DataTemplate DataType="{x:Type local:InteractiveRebaseModeInfo}">
                                        <Grid Height="20">
                                            <Grid.ColumnDefinitions>
                                                <ColumnDefinition Width="20"/>
                                                <ColumnDefinition Width="60"/>
                                                <ColumnDefinition Width="*"/>
                                            </Grid.ColumnDefinitions>
                                            <Path Grid.Column="0" Width="12" Height="12" Margin="4,0,0,0" Fill="{Binding Theme}" Style="{StaticResource Style.Icon}" Data="M 0,0 A 180,180 180 1 1 1,1 Z"/>
                                            <Label Grid.Column="1" Content="{Binding Title}" Padding="4,0"/>
                                            <Label Grid.Column="2" Content="{Binding Desc}" Foreground="{StaticResource Brush.FG2}" FontSize="11" Padding="4,0"/>
                                        </Grid>
                                    </DataTemplate>
                                </ComboBox.ItemTemplate>
                            </ComboBox>

                            <Grid Grid.Column="1">
                                <ContentControl x:Name="MessageEditorAnchor" MouseDoubleClick="PopupMessageEditor">
                                    <TextBlock Text="{Binding Subject, Mode=TwoWay}" VerticalAlignment="Center" Foreground="{StaticResource Brush.FG}"/>
                                </ContentControl>
                                
                                <Popup x:Name="MessageEditor" Placement="Bottom" IsOpen="{Binding IsEditorOpened}" VerticalOffset="1" Height="150" Width="400" PlacementTarget="{Binding ElementName=MessageEditorAnchor}">
                                    <Border BorderBrush="{StaticResource Brush.Accent1}" BorderThickness="1" Background="{StaticResource Brush.BG4}">
                                        <Grid Margin="8">
                                            <Grid.RowDefinitions>
                                                <RowDefinition Height="*"/>
                                                <RowDefinition Height="32"/>
                                            </Grid.RowDefinitions>
                                            <Border Grid.Row="0" BorderBrush="{StaticResource Brush.Border1}" BorderThickness="1">
                                                <Grid>
                                                    <Grid.RowDefinitions>
                                                        <RowDefinition Height="24"/>
                                                        <RowDefinition Height="1"/>
                                                        <RowDefinition Height="79"/>
                                                    </Grid.RowDefinitions>

                                                    <TextBox Grid.Row="0" Text="{Binding EditSubject, Mode=TwoWay}" Height="32" Padding="2,0" helpers:TextBoxHelper.Placeholder="Enter commit subject" BorderThickness="0"/>
                                                    <Rectangle Grid.Row="1" Height="1" Fill="{StaticResource Brush.FG}" Opacity="0.1"/>
                                                    <TextBox Grid.Row="2" Text="{Binding EditMessage, Mode=TwoWay}" TextChanged="CommitMessageChanged" Height="79" ScrollViewer.VerticalScrollBarVisibility="Auto" Padding="2" helpers:TextBoxHelper.Placeholder="Enter commit description. Optional" helpers:TextBoxHelper.PlaceholderBaseline="Top" BorderThickness="0" AcceptsReturn="True" AcceptsTab="True" TextWrapping="Wrap"/>
                                                </Grid>
                                            </Border>

                                            <StackPanel Grid.Row="1" Orientation="Horizontal" HorizontalAlignment="Right" Margin="0,8,0,0">
                                                <Button Click="ApplyMessageEdit" Height="24" Style="{StaticResource Style.Button.AccentBordered}" BorderThickness="1" Content="APPLY" Margin="8,0"/>
                                                <Button Click="HideMessageEditor" Height="24" Style="{StaticResource Style.Button.Bordered}" BorderThickness="1" Content="CANCEL"/>
                                            </StackPanel>
                                        </Grid>
                                    </Border>
                                </Popup>
                            </Grid>
                            
                            <TextBlock
                                Grid.Column="2"
                                Foreground="{StaticResource Brush.FG}"
                                VerticalAlignment="Center"
                                Text="{Binding Commit.Committer.Name}"/>

                            <TextBlock
                                Grid.Column="3"
                                Foreground="{StaticResource Brush.FG}"
                                VerticalAlignment="Center"
                                Text="{Binding Commit.Committer.Time}"/>

                            <StackPanel
                                Grid.Column="4"
                                Orientation="Horizontal"
                                Margin="4,0">
                                <Button Click="MoveUp" ToolTip="MOVE UP">
                                    <Path Width="12" Height="12" Style="{StaticResource Style.Icon}" Data="{StaticResource Icon.MoveUp}"/>
                                </Button>

                                <Button Click="MoveDown" ToolTip="MOVE DOWN" Margin="4,0,0,0">
                                    <Path Width="12" Height="12" Style="{StaticResource Style.Icon}" Data="{StaticResource Icon.MoveDown}"/>                                    
                                </Button>
                            </StackPanel>
                        </Grid>
                    </DataTemplate>
                </ListView.ItemTemplate>
            </ListView>

            <!-- Loading Tip -->
            <Grid Grid.Row="1" IsHitTestVisible="False">
                <!-- Loading tip -->
                <Path x:Name="loading" Grid.ColumnSpan="5" Data="{StaticResource Icon.Loading}" RenderTransformOrigin=".5,.5" Visibility="Hidden">
                    <Path.RenderTransform>
                        <RotateTransform Angle="0"/>
                    </Path.RenderTransform>

                    <Path.Style>
                        <Style BasedOn="{StaticResource Style.Icon}" TargetType="{x:Type Path}">
                            <Setter Property="Width" Value="48"/>
                            <Setter Property="Height" Value="48"/>
                            <Setter Property="HorizontalAlignment" Value="Center"/>
                            <Setter Property="VerticalAlignment" Value="Center"/>
                            <Setter Property="Fill" Value="{StaticResource Brush.FG2}"/>
                        </Style>
                    </Path.Style>
                </Path>
            </Grid>
            
            <!-- Splitter -->
            <GridSplitter Grid.Row="2" HorizontalAlignment="Stretch" VerticalAlignment="Stretch" Background="Transparent"/>

            <!-- Commit Detail -->
            <local:CommitViewer x:Name="commitViewer" Grid.Row="3" Background="{StaticResource Brush.BG4}"/>
            
            <!-- Options Bar -->
            <Grid Grid.Row="5">
                <Grid.ColumnDefinitions>
                    <ColumnDefinition Width="*"/>
                    <ColumnDefinition Width="80"/>
                    <ColumnDefinition Width="8"/>
                    <ColumnDefinition Width="80"/>
                    <ColumnDefinition Width="4"/>
                </Grid.ColumnDefinitions>

                <StackPanel Grid.Column="0" Orientation="Horizontal" Margin="4,0,24,0">
                    <Label Grid.Column="0" Content="Rebase :"/>
                    <Path Style="{StaticResource Style.Icon}" Data="{StaticResource Icon.Branch}"/>
                    <Label x:Name="branch"/>
                    
                    <Label Grid.Column="2" Content="On :" Margin="8,0,0,0"/>
                    <Path Style="{StaticResource Style.Icon}" Data="{StaticResource Icon.Commit}"/>
                    <Label x:Name="on"/>
                </StackPanel>

                <Button Grid.Column="1" Height="26" Click="Start" Style="{StaticResource Style.Button.AccentBordered}" Content="REBASE"/>
                <Button Grid.Column="3" Height="26" Click="Cancel" Style="{StaticResource Style.Button.Bordered}" Content="CANCEL"/>
            </Grid>
        </Grid>
    </Border>
</Window>
