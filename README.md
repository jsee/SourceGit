# SourceGit

开源的Git客户端，仅用于Windows 10。单文件，无需安装，< 500KB。

## 预览

* DarkTheme

![Theme Dark](./screenshots/theme_dark.png)

* LightTheme

![Theme Light](./screenshots/theme_light.png)


## Thanks

* [PUMA](https://gitee.com/whgfu) 配置默认User
